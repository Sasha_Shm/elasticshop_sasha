# coding=utf-8

from .views import *
from django.conf.urls import url, include
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^choose_country/$', ChooseCountryView.as_view(), name="choose-country-url"),
    url(r'^choose_another_country/$', ChooseAnotherCountryView.as_view(), name="choose-another-country-url"),
    url(r'^core/$', TemplateView.as_view(template_name="core.html")),
]