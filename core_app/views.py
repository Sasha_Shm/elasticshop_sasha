# -*- coding: utf-8 -*-# -*- coding: utf-8 -*-


from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.utils import translation
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView

from core_app.models import Country, Language
from django.utils.translation import (
    LANGUAGE_SESSION_KEY, check_for_language, get_language, to_locale,
)

class ChooseCountryView(TemplateView):

    template_name = 'choose_country.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ChooseCountryView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ChooseCountryView, self).get_context_data(**kwargs)
        context['countries'] = Country.get_available_countries()
        context['languages'] = Language.get_available_languages()
        return context

    def post(self, request, *args, **kwargs):
        from django.utils import translation
        code = request.POST.get('language')
        request.session[LANGUAGE_SESSION_KEY] = code
        request.session['django_language'] = code
        if translation.check_for_language(code):
            translation.activate(code)
        request.session[translation.LANGUAGE_SESSION_KEY] = code
        request.session['country'] = request.POST.get('country')
        request.session['language'] = request.POST.get('language')
        return redirect(request.GET.get('path', '/'))


class ChooseAnotherCountryView(TemplateView):

    template_name = 'choose_another_country.html'

    def post(self, request, *args, **kwargs):
        print(request.POST.get('language'))
        return redirect(request.GET.get('path', '/'))
