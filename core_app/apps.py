# -*- coding: utf-8 -*-


from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core_app'
    verbose_name = "Базовое приложение"
