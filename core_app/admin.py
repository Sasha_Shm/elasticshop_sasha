# -*- coding: utf-8 -*-


from django.contrib import admin
from core_app.models import Country, Language


admin.site.register(Country)
admin.site.register(Language)
