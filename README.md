### [ForceShop](https://github.com/dlyapun/django-force-shop) - alpha version ###

### About ###

Light and powerfull e-commerce aplication for django!

New tasks:
1. Добавить регистрацию профиля.
2. Регистрация через соц-сети
3. Доработать корзину
4. Сделать сортировку
5. 

Installation:

{% load force_shop_tags %}

Usage Example:

for list template:

Product:
	{{ product.name }}
	{{ product.brand.name }}
	{{ product.get_absolute_url }}
	{{ product.product_image }}
	{{ product.product_price }}
	{{ product.product_price_sale }}
	{{ product.short_desc }}
	{{ product.text }}

ProductVariations:
	{{ product_variation.product.name }}

	# Special label
	{{ product_variation.default }}
	{{ product_variation.discount }}
	{{ product_variation.new_product }}


	{{ product_variation.count }}
	{{ product_variation.guarantee }}

	{{ product_variation.image.image.url }}

	# Use only this func for view price
	{{ product_variation.product_price_sale }}
	{{ product_variation.product_price }}

	{% for color in product_variation.colors.all %}
    	<p>{{ color.name }}</p>
    {% endfor %} 

    {% for variation in product_variation.variations.all %}
    	<p>{{ variation.name }}</p>
    {% endfor %} 

    # Form Count ProductVariations
    <select id="id_quantity" name="quantity" class="form-control">
        {% for i in product_variation.count|create_range %}
            <option value="{{ i }}">{{ i }}</option>
        {% endfor %}
    </select>

CART TEMPLATE:
	{{ total_price }} 

	{% for item in cart %}
		{{ item.product.product.name }}
		{{ item.product.product.short_desc }}
		{{ item.quantity }}
		{{ item.total_price }}
		{{ item.product.image.image.url }}
		<a href="{{ item.product.get_remove_url }}">Remove</a>
    {% endfor %}