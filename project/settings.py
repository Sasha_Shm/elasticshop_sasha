import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_NAME = os.path.basename(BASE_DIR)

SECRET_KEY = 'eaft^&#exs*(r9l&p&e^ba&8%=1&$2e!b3u6-8^8%0(=ir(^)g'

DEBUG = True


### PROJECT SETTINGS ##############################
ADMINS = [('Dmytro Lyapun', 'dlyapun@gmail.com')]
MANAGERS = ADMINS
SITE_URL = 'localhost:8000'
SITE_NAME = 'ElasticShop'
NO_REPLY_EMAIL_ADDRESS = 'no-reply@example.com'
ADMIN_EMAIL_ADDRESS = 'admin@example.com'
SUPPORT_EMAIL_ADDRESS = 'support@example.com'
SWITCH_HASH_KEY = 'user_switch'
###################################################

ALLOWED_HOSTS = [
    '127.0.0.1:8000',
    '127.0.0.1',
    '.example.com',
]
INTERNAL_IPS = ('127.0.0.1')


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'debug_toolbar',
    'force_shop',
    'profile',
    'accounts',
    'mptt',
]

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'project.urls'
WSGI_APPLICATION = 'project.wsgi.application'


TEMPLATE_DIRS_APPS = [os.path.join(BASE_DIR, 'templates')]
for root, dirs, files in os.walk(BASE_DIR):
    if 'templates' in dirs:
        TEMPLATE_DIRS_APPS += (os.path.join(root, 'templates'),)


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': TEMPLATE_DIRS_APPS,
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': True,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'force_shop.context_processor.contex_force_shop',
            ],
        },
    },
]


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_DIR, 'db.sqlite3'),
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
]

AUTH_USER_MODEL = 'accounts.User'

LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
    ('es', 'Spanish'),
)

LANGUAGE_CODE = 'ru'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


STATIC_URL = '/static/'
MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
STATIC_ROOT = os.path.join(BASE_DIR, 'staticstorage')

STATICFILES_DIRS = [
    os.path.join(PROJECT_DIR, "static"),
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

DEFAULT_IMAGE_PATH = '%s/force_shop_media/images/product/default.png' % MEDIA_URL

DJANGORESIZED_DEFAULT_SIZE = [1920, 1080]
DJANGORESIZED_DEFAULT_QUALITY = 75
DJANGORESIZED_DEFAULT_KEEP_META = True
DJANGORESIZED_DEFAULT_FORCE_FORMAT = 'JPEG'


### PROJECT SETTINGS ##############################
ADMINS = [('Dmytro Lyapun', 'dlyapun@gmail.com')]
MANAGERS = ADMINS
SITE_URL = 'localhost:8000'
SITE_NAME = 'ElasticShop'
SITE_LOGO = STATIC_URL + 'img/logo.png'
SITE_ICON = STATIC_URL + 'img/favicon.png'
NO_REPLY_EMAIL_ADDRESS = 'no-reply@example.com'
ADMIN_EMAIL_ADDRESS = 'admin@example.com'
SUPPORT_EMAIL_ADDRESS = 'support@example.com'
SWITCH_HASH_KEY = 'user_switch'
###################################################

try:
    from .local_settings.settings import *
except ImportError:
    pass
