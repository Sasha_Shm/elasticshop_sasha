const urlAPI = "/shop/api/v1/";
const getProductsListURL = urlAPI + "products-list/";
const addProductURL = urlAPI + "product-add/";

Vue.http.interceptors.push(function (request, next) {
    request.headers['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
    next();
});

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

var app = new Vue({
    el: '#all',
    data: {
        message: 'Hello Vue!',
        markers: [],
        map: null,
        products: [{}],
    },
    methods: {
        getProductsList: function () {
            var _this = this;
            var url = getProductsListURL + "?" + window.location.search.substr(1);
            fetch(url).then(function (response) {
                if (response.ok) { return response.json(); }
                throw new Error('Network response was not ok');
            }).then(function (response) {
                _this.products = response;
            }).catch(function (error) {
                console.log("getProductsList", error);
            });
        },

        addProduct: function (product_id) {
            var _this = this;
            console.log('product_id', product_id);
            _this.$http.post(addProductURL, {'id': product_id}).then(function (response) {
                console.log(response.data);
            });
        },
    }
});



app.getProductsList();