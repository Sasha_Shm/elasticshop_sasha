# coding=utf-8
# author: dlyapun

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render_to_response
from django.views import View

from .cart import Cart
from .forms import AddProductForm, OrderForm
from .models import Product, Order, Category, ProductVariation, Shipping

from django.shortcuts import redirect
from django.views.generic import TemplateView, DetailView, FormView
from django.views.decorators.cache import never_cache


DISABLE = 0
ENABLE = 1

LIMIT_OBJECTS = 72


class BaseProductView(TemplateView):
    template_name = 'force_shop/list_shop.html'

    @never_cache
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(BaseProductView, self).get_context_data(**kwargs)

        request_count = self.request.GET.get('count', '12')
        products_count = int(request_count) if request_count.isdigit() else LIMIT_OBJECTS

        sorted_field = self.request.GET.get('s', 'rating')
        context['sort_by'] = sorted_field
        context['products'] = Product.get_sorted_products(sorted_field)[:products_count]
        context['path_products_count_in_page'] = products_count
        return context


class ListProductView(BaseProductView):
    pass


class CategoryListView(TemplateView):
    template_name = 'force_shop/list_shop.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        request_count = self.request.GET.get('count', 12)
        sorted_field = self.request.GET.get('s', 'rating')
        category = kwargs.get('category_slug')

        # products_count = int(request_count) if request_count.isdigit() else LIMIT_OBJECTS
        context['products'] = Product.get_products_by_category(category)
        context['category'] = Category.get_by_slug(category)
        return context


class DetailProductView(DetailView):
    model = Product
    context_object_name = 'product'
    template_name = 'force_shop/detail_shop.html'

    def get_object(self, queryset=None):
        return Product.get_by_slug(self.kwargs.get('meta_url'))

    def get_context_data(self, **kwargs):
        context = super(DetailProductView, self).get_context_data(**kwargs)
        context['product_variations'] = self.object.variations.all()
        return context


class AddToCartView(FormView):
    """Add to cart logic"""
    form_class = AddProductForm
    success_url = '/shop/cart'

    def form_valid(self, form):
        # get item quantity and find product by id then add to cart
        quantity = form.cleaned_data['quantity']
        product = ProductVariation.get_by_id(self.request.POST['product_id'])   # get product by id
        Cart(self.request).add(product, quantity)
        return super(AddToCartView, self).form_valid(form)


class RemoveProductFromCart(View):

    def get(self, *args, **kwargs):
        product = ProductVariation.get_by_id(kwargs.get('product_id'))
        cart = Cart(self.request)
        cart.remove(product)
        return redirect('/shop/cart/')


class CartView(DetailView):

    model = Cart
    context_object_name = 'cart'
    template_name = 'force_shop/cart.html'

    def get_object(self, queryset=None):
        return Cart(self.request)


class OrderView(FormView):

    template_name = 'force_shop/order.html'
    form_class = OrderForm

    def get_initial(self):
        initial = super(OrderView, self).get_initial()
        if self.request.user.is_authenticated:
            initial['order_first_name'] = self.request.user.first_name
            initial['order_last_name'] = self.request.user.last_name
            initial['order_email'] = self.request.user.email
            # TODO: Improve Address field for user where we can store billing informatin and phone
        return initial

    def get_context_data(self, **kwargs):
        context = super(OrderView, self).get_context_data(**kwargs)
        context['cart'] = Cart(self.request)
        context['shippings'] = Shipping.get_enable_shippings()
        return context

    def post(self, *args, **kwargs):
        form = OrderForm(self.request.POST)  # Проверить
        if form.is_valid():
            user_id = self.request.user.id if self.request.user.is_authenticated else None
            cart = Cart(self.request).get_cart(self.request)
            cart.save()
            form.save(cart=cart, user_id=user_id)
            Cart(self.request).new(self.request)
            url = '/shop/pay/'
            return redirect(url)
        return super(OrderView, self).post(*args, **kwargs)


class PayOrderView(TemplateView):
    template_name = 'force_shop/pay.html'

    def get_context_data(self, **kwargs):
        context = super(PayOrderView, self).get_context_data(**kwargs)
        context['form'] = OrderForm()
        return context


class SearchProductView(BaseProductView):
    template_name = 'force_shop/list_shop.html'

    def get_queryset(self):
        qs = Product.get_default_query()

        keywords = self.request.GET.get('q')
        if keywords:
            # TODO: use postgres for local dev
            # full text search for postgres db
            # query = SearchQuery(keywords)
            # vector = SearchVector('name', 'text')
            # qs = qs.annotate(search=vector).filter(search=query)
            # qs = qs.annotate(rank=SearchRank(vector, query))
            qs = qs.filter(Q(name__icontains=keywords) | Q(text__icontains=keywords))
        return qs

    def get_context_data(self, **kwargs):
        context = super(SearchProductView, self).get_context_data(**kwargs)
        context['products'] = self.get_queryset()
        return context
