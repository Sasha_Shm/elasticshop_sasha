from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

from force_shop.models import Product
from force_shop.serializers import ProductSerializer


class ProductListView(generics.ListAPIView):
    queryset = Product.get_enable_products()
    serializer_class = ProductSerializer

    def get_queryset(self):

        return Product.get_enable_products()


class ProductAddView(APIView):

    def post(self, request, *args, **kwargs):

        return Response("Success.", status=status.HTTP_200_OK)


class ProductRemoveView(APIView):

    def post(self, request, *args, **kwargs):

        return Response("Success.", status=status.HTTP_200_OK)