# coding=utf-8
# author: dlyapun
from django.conf.urls import url
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404

from accounts.models import User
from .models import *
from django.forms import CheckboxSelectMultiple
from mptt.admin import MPTTModelAdmin, DraggableMPTTAdmin, TreeRelatedFieldListFilter

import logging

logger = logging.getLogger(__name__)

def report_field_wrapper(text):
    def inner_report_field_wrapper(f):

        def wrapper(*args, **kw):

            try:
                return f(*args, **kw)
            except Exception as e:
                logger.exception("Error getting data for" + text)
                return "error"
        wrapper.allow_tags = True
        wrapper.short_description=text
        return wrapper

    return inner_report_field_wrapper


class ProductVariationInline(admin.StackedInline):
    # Filter for ManytoMany fields
    filter_horizontal = ('variations',
                         'colors')
    # Variable view fields in admin-interface
    fieldsets = (
        (None, {
            'fields': (('state', 'default', 'discount',
                        'new_product', 'guarantee'),
                       ('currency', 'count', 'price', 'price_sale'),
                       ('code', 'image')
                       )
        }),
        ('Advanced options', {
            'classes': ('',),
            'fields': ('colors', 'variations')
        }),
    )
    # Our parent for StackedInline
    model = ProductVariation
    # For state filds - select
    radio_fields = {"state": admin.VERTICAL}

    # Product variations count
    extra = 1

    def __init__(self, *args, **kwargs):
        super(ProductVariationInline, self).__init__(*args, **kwargs)
        # self.fields['variations'].queryset = Tag.get_avialable_tags_by_product(self.initial)

    def get_formset(self, request, obj=None, **kwargs):
        self.instance = obj
        return super(ProductVariationInline, self).get_formset(request, obj, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "variations":
            kwargs["queryset"] = Tag.get_avialable_tags_by_product(self.instance)
        return super(ProductVariationInline, self).formfield_for_manytomany(db_field, request, **kwargs)


class ProductAdmin(admin.ModelAdmin):
    filter_horizontal = ('category',
                         'related_products',
                         'upsell_products',
                         'images')
    list_display = ['name', 'state', 'purchase_state', 'brand', 'product_view', 'currency', 'price', 'price_sale', 'new_product', 'default', 'rating']
    list_editable = ['state', 'price', 'brand', 'price_sale', 'currency', 'new_product', 'default', 'rating']
    ordering = ('-date_creation',)
    # Trenslate title to slug_url
    prepopulated_fields = {"meta_url": ("name",)}
    # For state filds - select
    radio_fields = {"state": admin.VERTICAL}
    # Add button "Save as"
    save_as = True
    # For our child model
    inlines = [
        ProductVariationInline,
    ]

    fieldsets = (
        (None, {
            'fields': (('name', 'short_desc', 'rating'),
                       ('price', 'price_sale', 'currency'),
                       ('brand', 'image', ),
                       ('category', 'images', 'related_products', 'upsell_products',),
                       )
        }),
        ('START OF SALES', {
            'classes': ('collapse',),
            'fields': (('date_start', 'data_end'),
                       )
        }),
        ('META INFORMATION', {
            'classes': ('collapse',),
            'fields': (('meta_title', 'meta_url'),
                       ('meta_description', 'meta_keywords'),
                       )
        }),
    )

    def save_related(self, request, form, formsets, change):
        """
            Функция которая устанавливает все родительские категории к продукту
        """
        super(ProductAdmin, self).save_related(request, form, formsets, change)
        # Берем текущий продукт и все его категории
        product = form.instance
        for category in product.category.all():
            # Добавляем всех родителей
            product.category.add(*category.get_ancestors())


class ProductVariationAdmin(admin.ModelAdmin):
    filter_horizontal = ('variations', 'colors',)
    # formfield_overrides = {
    #     Variation: {'widget': CheckboxSelectMultiple},
    # }

    def get_form(self, request, obj=None, **kwargs):
        form = super(ProductVariationAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['variations'].widget = CheckboxSelectMultiple()
        return form


class TagCollectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'tag_collection_view')
    filter_horizontal = ('variations',)


class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'image_view')


class ImagesProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'image_view')


class TagAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ColorAdmin(admin.ModelAdmin):
    list_display = ('color_view', 'name', 'hex_code')


class ShippingAdmin(admin.ModelAdmin):
    list_display = ('name', 'image_view')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'client_name', 'order_phone', 'price_captured', 'shipping_tax_price', 'total_price', 'status_payment', 'status_shipping', 'shipping_type', )
    readonly_fields = ['shipping_type']

    fieldsets = (
        (None, {
            'fields': ()
        }),
    )

    def get_urls(self):
        urls = super(OrderAdmin, self).get_urls()
        order_admin_urls = [
            url(r'^action/(?P<pk>\d+)/$', self.action_view),
            # url(r'^export_csv/$', self.export_csv),
            # url(r'^export_shipping_label_csv/$', self.export_shipping_label_csv)
        ]
        return order_admin_urls + urls

    def action_view(self, request, pk):
        status = request.GET['value']
        amount = request.GET.get('amount', 0)
        order = get_object_or_404(Order, pk=pk)

        if status == 'refund':
            self.refund_order(request, order, amount)
            return HttpResponseRedirect(request.META["HTTP_REFERER"])

        if status == 'calculate':
            order.calculate_prices()
            return HttpResponseRedirect(request.META["HTTP_REFERER"])

        if status == 'cancel':
            order.cancel()
            return HttpResponseRedirect(request.META["HTTP_REFERER"])

        try:
            eval("order." + status + "(test=False)")
        except AttributeError:
            eval("self." + status + "(request, [order])")
            raise Exception("No valid status")
        return HttpResponseRedirect(request.META["HTTP_REFERER"])


class CartAdmin(admin.ModelAdmin):
    list_display = ('id', 'creation_date', 'total_price')


admin.site.register(Product, ProductAdmin)
admin.site.register(ProductVariation, ProductVariationAdmin)
admin.site.register(TagCollection, TagCollectionAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Category, DraggableMPTTAdmin, list_display = ('tree_actions', 'indented_title',), list_display_links = ('indented_title',), prepopulated_fields = {"slug": ("name",)})
admin.site.register(Cart, CartAdmin)
admin.site.register(Color, ColorAdmin)
admin.site.register(ImagesProduct, ImagesProductAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Currency)
admin.site.register(Shipping, ShippingAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Item)

# admin.site.register(Item)
# admin.site.register(Item)


