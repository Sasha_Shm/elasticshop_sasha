# coding: utf-8

from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.core.exceptions import ObjectDoesNotExist
from ..models import Currency, Color, TagCollection
from django.core import urlresolvers

register = template.Library()


@register.simple_tag()
def url_to_admin(obj):
    url = urlresolvers.reverse('admin:%s_%s_change' % (obj._meta.app_label,
                                                       obj._meta.module_name),
                                                       args=[obj.id])
    return url


@register.simple_tag()
def colors_template():
    colors = Color.objects.all()
    url = template.loader.get_template("force_shop/template_tag_colors.html")
    data = {'colors': colors}
    return url.render(Context(data))


@register.simple_tag()
def variations_template_tag(request):
    variations = TagCollection.objects.all()
    url = template.loader.get_template("force_shop/template_tag_variations.html")
    data = {'variations': variations}
    return url.render(Context(data))


@register.filter
def create_range(value, start_index=1):
    return list(range(start_index, value+start_index))
