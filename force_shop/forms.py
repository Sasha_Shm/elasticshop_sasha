# coding=utf-8
# author: dlyapun

from django import forms
from django.forms import ModelForm
from .models import Order, Currency


class AddProductForm(forms.Form):
    quantity = forms.IntegerField()


class OrderForm(ModelForm):
    class Meta:
        model = Order
        exclude = ['user_id' ,'date_creation', 'cart', 'transaction_id', 'status_payment', 'status_shipping', 'price_captured', 'shipping_price', 'tax_price', 'total_price', 'currency']

    def save(self, *args, **kwargs):
        cart = kwargs.pop('cart')
        user_id = kwargs.pop('user_id')
        obj = super(OrderForm, self).save(commit=False)
        obj.user_id = user_id
        obj.currency = Currency.get_default()
        obj.cart = cart
        obj.save()
        obj.calculate_prices()
        return obj
