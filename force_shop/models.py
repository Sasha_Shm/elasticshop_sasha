# coding=utf-8
# author: dlyapun

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext as _
from django.utils.translation import (ugettext_lazy as _, pgettext_lazy as __)
from django_resized import ResizedImageField
from mptt.models import MPTTModel, TreeForeignKey

from project.settings import DEFAULT_IMAGE_PATH

DISABLE = 0
ENABLE = 1

STATE_CHOICE = (
    (ENABLE, _('Product enable')),
    (DISABLE, _('Product disable')),
)


class Product(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("Product name"))
    text = models.TextField(verbose_name=_('Product description'), blank=True, null=True,)

    price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name=_('Min price'))
    price_sale = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name=_('Sale price'))
    currency = models.ForeignKey('Currency', verbose_name=_('Currency'))

    # TODO: Delete field
    default = models.BooleanField(default=False, verbose_name=_('Product default'))
    discount = models.BooleanField(default=False, verbose_name=_('Discount'))
    new_product = models.BooleanField(default=False, verbose_name=_('New product'))

    short_desc = models.CharField(max_length=250, blank=True, null=True, verbose_name=_('Short description'))
    state = models.SmallIntegerField(default=ENABLE, choices=STATE_CHOICE, verbose_name=_('Status'))

    # ???????
    purchase_state = models.BooleanField(default=True, verbose_name=_('Purchase state'))

    date_creation = models.DateTimeField(auto_now_add=True)
    date_start = models.DateTimeField(verbose_name=_('Date start'), blank=True, null=True,)
    data_end = models.DateTimeField(verbose_name=_('Date end'), blank=True, null=True,)

    category = models.ManyToManyField('Category', blank=True,
                                      verbose_name=_('Categories'),
                                      related_name="product_category")

    # ======= META INFORMATION ABOUT PRODUCT ======= #
    meta_title = models.CharField(max_length=100, verbose_name=_('Meta Title'), blank=True, null=True,)
    meta_url = models.CharField(max_length=100, verbose_name=_('Meta url'),)
    meta_description = models.CharField(max_length=500, verbose_name=_('Meta descriptoin'), blank=True, null=True,)
    meta_keywords = models.CharField(max_length=100, verbose_name=_('Meta Keywords'), blank=True, null=True,)
    # ============================================== #

    related_products = models.ManyToManyField('self', symmetrical=True, blank=True, verbose_name=_('Related products'),
                                              related_name="related_products_")
    upsell_products = models.ManyToManyField('self', symmetrical=True, blank=True, verbose_name=_('Upsell products'),
                                             related_name="upsell_products_")

    brand = models.ForeignKey('Brand', blank=True, null=True,)

    rating = models.FloatField(default=0.0, verbose_name=_('Rating'))

    image = ResizedImageField(upload_to='force_shop_media/images/product/',
                              blank=True, null=True, verbose_name=_('Image'))
    images = models.ManyToManyField('ImagesProduct', blank=True, verbose_name=_('Images'), related_name="product_images")

    class Meta:
        ordering = ["-name"]
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return '%s' % (self.name)

    def get_absolute_url(self):
        return '/shop/%s' % self.slug

    @property
    def default_product(self):
        product = self.variations.filter(default=True).first()
        if not product:
            product = self.variations.first()
        return product

    @property
    def slug(self):
        return "{0}-{1}".format(self.id, self.meta_url)

    def product_price(self):
        default_currency = DEFAULT_CURRENCY
        price = self.price
        if default_currency != self.currency:
            price = round(self.price * default_currency.coefficient, 2)
        return price

    def product_price_sale(self):
        default_currency = DEFAULT_CURRENCY
        price_sale = self.price_sale
        if default_currency != self.currency:
            price_sale = round(self.price_sale * default_currency.coefficient, 2)

        return default_currency.symbol + ' ' + str(price_sale)

    @property
    def product_image(self):
        return self.image.url if self.image else DEFAULT_IMAGE_PATH

    def product_view(self):
        return '<img src="%s" width="100px"/>' % self.image.url if self.image else DEFAULT_IMAGE_PATH
    product_view.short_description = _('Image')
    product_view.allow_tags = True

    @staticmethod
    def get_default_query():
        return Product.objects.filter().select_related('brand', 'currency').prefetch_related('variations', 'variations__image')

    @staticmethod
    def get_enable_products():
        return Product.get_default_query().filter(state=ENABLE)

    @staticmethod
    def get_by_slug(slug):
        return Product.objects.get(id=slug.split('-')[0])

    @staticmethod
    def get_sorted_products(sort_field):
        sort_field = Product.get_sorted_field(sort_field)
        return Product.get_enable_products().order_by(sort_field)

    @staticmethod
    def get_products_by_category(category):
        if type(category) == str or type(category) == unicode:
            category = Category.get_by_slug(category)
        return Product.get_enable_products().filter(category=category)

    @staticmethod
    def get_sorted_field(sort_field):
        sorted_fields = {
            'name': 'name',
            'rname': "-name",
            'low': "min_price",
            'high': '-min_price',
            'rating': '-rating',
            'date': '-date_creation'
        }
        return sorted_fields.get(sort_field, 'rating')


class ProductVariation(models.Model):
    product = models.ForeignKey('Product', verbose_name=_('Main product'), related_name="variations")

    state = models.SmallIntegerField(default=ENABLE, choices=STATE_CHOICE, verbose_name=_('Status'))

    # TODO: delete this fields
    default = models.BooleanField(default=False, verbose_name=_('Product default'))
    discount = models.BooleanField(default=False, verbose_name=_('Discount'))
    new_product = models.BooleanField(default=False, verbose_name=_('New product'))

    code = models.CharField(max_length=128, verbose_name=_('Product code'), blank=True, null=True,)

    count = models.SmallIntegerField(verbose_name=_('Count'), default=1,)

    price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name=_('Price'))
    price_sale = models.DecimalField(default=0.00,  max_digits=10,  decimal_places=2, verbose_name=_('Sale price'))
    currency = models.ForeignKey('Currency', verbose_name=_('Currency'))

    # TODO: remove to Product Model
    guarantee = models.SmallIntegerField(verbose_name=_('Guarantee'), blank=True, null=True,)
    image = models.ForeignKey('ImagesProduct', blank=True, null=True, verbose_name=_('Image'))

    variations = models.ManyToManyField('Tag', blank=True, verbose_name=_('Variations'),
                                        related_name="variations_for_products")

    colors = models.ManyToManyField('Color', blank=True, verbose_name=_('Colors'),
                                    related_name="product_colors")

    class Meta:
        verbose_name = _('Subset of product')
        verbose_name_plural = _('Subset of products')

    def __str__(self):
        return '%s - %s%s' % (self.product.name,
                               self.currency.symbol,
                               self.price,)

    def __str__(self):
        return '%s - %s' % (self.product.name,
                             self.price,)

    def product_price(self):
        price = self.price
        default_currency = DEFAULT_CURRENCY

        if default_currency != self.currency:
            price = round(self.price * default_currency.coefficient, 2)
        return price

    def product_price_sale(self):
        price = self.price_sale
        default_currency = DEFAULT_CURRENCY

        if default_currency != self.currency:
            price = round(self.price_sale * default_currency.coefficient, 2)
        return price

    def get_add_url(self):
        return '/shop/add/%s' % self.id

    def get_remove_url(self):
        return '/shop/remove/%s' % self.id

    def current_image(self):
        if self.image:
            return self.image.image.url
        else:
            return self.product.image.url

    @staticmethod
    def get_by_id(id):
        return ProductVariation.objects.filter(id=id).select_related(
                                                      'product', 'currency', 'image').prefetch_related(
                                                      'variations', 'variations__image', 'colors').first()


class Currency(models.Model):
    """Валюта"""

    name = models.CharField(max_length=50, verbose_name=_('Name of currency'))
    coefficient = models.DecimalField(default=0.00, max_digits=10, decimal_places=4, verbose_name=_('Ratio to the dollar'))
    symbol = models.CharField(max_length=50, verbose_name=_('Symbol'))
    ico_code = models.CharField(max_length=3, verbose_name=_('Ico code'))
    default = models.BooleanField(default=False, verbose_name=_('Currency default'))

    class Meta:
        verbose_name = _('Currency')
        verbose_name_plural = _('Currency')

    def __str__(self):
        return '%s %s %s %s' % (self.symbol, self.name, self.coefficient, self.default,)

    def save(self, force_insert=False, force_update=False, using=None):
        if self.default is True:
            Currency.objects.all().update(default=False)
        super(Currency, self).save()

    @staticmethod
    def get_default():
        return Currency.objects.get(default=True)


class TagCollection(models.Model):
    name = models.CharField(max_length=50)
    variations = models.ManyToManyField('Tag', verbose_name=_('Tag collections'), related_name="variation_detail", blank=True)

    class Meta:
        verbose_name = _('Tag collection')
        verbose_name_plural = _('Tags collections')

    def tag_collection_view(self):
        return "\n".join([p.name for p in self.variations.all()])

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('Tag name'))

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    def get_absolute_url(self):
        return '/shop/tag/%s' % self.id

    def __str__(self):
        return self.name

    @property
    def parent_name(self):
        return self.variation_detail.first().name

    @staticmethod
    def get_by_ids(ids):
        return Tag.objects.filter(id__in=ids)

    @staticmethod
    def get_avialable_tags_by_product(product):
        categories = product.category.all()
        collections = []
        for category in categories:
            collections += category.collection.all()

        tags_ids = []
        for collection in collections:
            tags_ids += collection.variations.values_list('id', flat=True)
        return Tag.get_by_ids(tags_ids)


class Category(MPTTModel):
    name = models.CharField(max_length=50, verbose_name=_('Category name'))

    # TODO: Add uniue True to slug field and remove default value
    slug = models.CharField(max_length=50, verbose_name=_('Slug'), default="")
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    collection = models.ManyToManyField(TagCollection, verbose_name=_("Collections products"), blank=True)
    text = models.TextField(verbose_name=_('Description'), blank=True, null=True,)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categorys')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        trees = self.get_ancestors(ascending=False, include_self=True)
        url = [tree.slug for tree in trees]
        return "/".join(url)

    def get_products_count(self):
        return Product.objects.filter(category=self).count()

    def get_collections(self):
        return self.collection.all()

    @staticmethod
    def get_by_slug(slug):
        return Category.objects.filter(slug=slug).first()

    @staticmethod
    def get_roots():
        return Category.objects.filter(parent__isnull=True)


class Color(models.Model):
    """Цвета"""

    name = models.CharField(max_length=50, verbose_name=_('Color'))
    hex_code = models.CharField(max_length=6, verbose_name=_('HEX code'))

    class Meta:
        verbose_name = _('Color')
        verbose_name_plural = _('Colors')

    def color_view(self):
        if self.hex_code:
            return '<p style="width: 1px; height: 20px;background: #%s;-moz-border-radius: 50px;-webkit-border-radius: px;border-radius: 50px; border: 1px solid rgba(0, 0, 0, 0.29);"></p>' % self.hex_code
        else:
            return '(none)'
    color_view.short_description = _('Color')
    color_view.allow_tags = True

    def get_absolute_url(self):
        return '/shop/color/%s' % self.id

    def __str__(self):
        return '{0} - {1}'.format(self.name, self.hex_code)


class ImagesProduct(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    image = ResizedImageField(upload_to='force_shop_media/images/product/', quality=75,
                              blank=True, null=True, verbose_name=_('Image'))

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')

    def image_view(self):
        if self.image:
            return '<img src="%s" width="100px"/>' % self.image.url
        else:
            return '(none)'
    image_view.short_description = _('Image')
    image_view.allow_tags = True

    def __str__(self):
        return self.name


class Brand(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('Brand'))
    text = models.TextField(verbose_name=_('Description'),
                            blank=True, null=True,)
    image = ResizedImageField(upload_to='force_shop_media/images/brand/',
                              blank=True, null=True, quality=75,
                              verbose_name=_('Image'))

    class Meta:
        verbose_name = _('Brand')
        verbose_name_plural = _('Brands')

    def image_view(self):
        if self.image:
            url = '<img src="%s" width="100px"/>' % self.image.url
        else:
            url = '<img src="%sforce_shop_static/images/logos/brand_icon.png" width="100px"/>' % settings.STATIC_URL
        return url
    image_view.short_description = _('Image')
    image_view.allow_tags = True

    def __str__(self):
        return self.name


# TODO: Remove to order_models.py
class Order(models.Model):
    PAYMENT_CHOICE = (
        (1, _('Not paid')),
        (2, _('Paid')),
        (3, _('Refund')),
    )
    SHIPPING_CHOICE = (
        (1, _('In stock')),
        (2, _('On way')),
        (3, _('Delivered'))
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)

    order_first_name = models.CharField(_("First name"), max_length=100)
    order_last_name = models.CharField(_("Last name"), max_length=100)
    order_shipping = models.CharField(_("Address shipping company"), max_length=100, null=True, blank=True)
    order_flat = models.CharField(_("Flat"), max_length=100, null=True, blank=True)
    order_house = models.CharField(_("House"), max_length=100, null=True, blank=True)
    order_street = models.CharField(_("Street"), max_length=100, null=True, blank=True)
    order_city = models.CharField(_("City/Suburb"), max_length=100, null=True, blank=True)
    order_state = models.CharField(_("State/Region"), max_length=100, null=True, blank=True)
    order_postcode = models.CharField(_("Zip/Postcode"), max_length=10, null=True, blank=True)
    order_country = models.CharField(_("Country"), max_length=100, null=True, blank=True)
    order_phone = models.CharField(_("Phone"), max_length=20)
    order_email = models.EmailField(_("Email"), max_length=254)

    price_captured = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name=_('Price captured'))
    shipping_price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name=_('Shipping price'))
    tax_price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name=_('Tax price'))
    total_price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name=_('Tax price'))
    currency = models.ForeignKey('Currency', verbose_name=_('Currency'))

    additional_instructions = models.TextField(_("Additional instructions"), blank=True, null=True)
    date_creation = models.DateTimeField(_("Time"), auto_now_add=True)
    cart = models.ForeignKey('Cart', verbose_name=_("Cart object"))

    shipping_type = models.ForeignKey('Shipping', verbose_name=_('Shipping type'))
    transaction_id = models.CharField(_("Transaction ID"), max_length=255,null=True, blank=True)

    status_payment = models.PositiveIntegerField(default=1, choices=PAYMENT_CHOICE, verbose_name=_('Status payment'))
    status_shipping = models.PositiveIntegerField(default=1, choices=SHIPPING_CHOICE, verbose_name=_('Status shipping'))

    class Meta:
        verbose_name = __("commercial meaning", "Order")
        verbose_name_plural = __("commercial meaning", "Orders")
        ordering = ("-id",)

    def __str__(self):
        if self.id < 10:
            code = "000"
        elif self.id < 100:
            code = "00"
        else:
            code = "0"
        return "%s%s" % (code, self.id)

    def get_payment_label(self):
        dict = {
            1: 'label-warning',
            2: 'label-success',
            3: 'label-danger',
        }
        return dict.get(self.status_payment)

    def get_shipping_label(self):
        dict = {
            1: 'label-warning',
            2: 'label-info',
            3: 'label-success',
        }
        return dict.get(self.status_payment)

    def calculate_prices(self):
        self.price_captured = 0
        items = self.cart.all_cart_items()
        for item in items:
            self.price_captured += item.product.price * item.quantity
        self.save()

    @property
    def client_name(self):
        return "{0} {1}".format(self.order_first_name, self.order_last_name)

    @property
    def total_price(self):
        return self.price_captured + self.shipping_price + self.tax_price

    @property
    def total_quantity(self):
        return len(self.cart.all_cart_items())

    def shipping_tax_price(self):
        return "{0} | {1}".format(self.shipping_price, self.tax_price)
    shipping_tax_price.short_description = _('Shipping | Tax price')
    shipping_tax_price.allow_tags = True


class Shipping(models.Model):
    name = models.CharField(max_length=50)
    image = ResizedImageField(upload_to='force_shop_media/images/shipping/', quality=75,
                              blank=True, null=True, verbose_name=_('Image'), )

    class Meta:
        verbose_name = _('Shipping')
        verbose_name_plural = _('Shippings')

    def image_view(self):
        if self.image:
            return '<img src="%s" width="100px"/>' % self.image.url
        else:
            return '(none)'
    image_view.short_description = _('Image')
    image_view.allow_tags = True

    def __str__(self):
        return self.name

    @staticmethod
    def get_enable_shippings():
        return Shipping.objects.all()


# TODO: Remove to cart_models.py
class Cart(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Cart')
        verbose_name_plural = _('Carts')
        ordering = ('-creation_date',)

    def all_cart_items(self):
        items = Item.objects.filter(cart_id=self.id)
        return items

    def total_price(self):
        items = Item.objects.filter(cart=self)
        default_currency = Currency.objects.filter(default=True).first()
        if not default_currency:
            return "Set the default currency!"

        total_price = 0

        for item in items:
            if default_currency != item.product.currency:
                price = round(item.product.price * default_currency.coefficient, 2)
            else:
                price = round(item.product.price, 2)

            items_price = price * item.quantity
            total_price += items_price

        return default_currency.symbol + ' ' + str(total_price)
    total_price.short_description = _('Total price')
    total_price.allow_tags = True

    def __str__(self):
        return '%s - %s' % (self.id, self.creation_date)

    def __str__(self):
        return '%s - %s' % (self.id, self.creation_date)


class Item(models.Model):
    cart = models.ForeignKey(Cart, verbose_name=_('cart'))
    quantity = models.PositiveIntegerField(verbose_name=_('quantity'))
    product = models.ForeignKey('ProductVariation')

    class Meta:
        verbose_name = _('Item')
        verbose_name_plural = _('Items')
        ordering = ('cart',)

    def __str__(self):
        return '%d units of %s' % (self.quantity, self.product.product.name)

    @property
    def total_price(self):
        default_currency = Currency.objects.filter(default=True).first()
        if not default_currency:
            return self.product.currency.symbol + ' ' + str(self.product.price)
        price = self.product.price * self.quantity
        if default_currency != self.product.currency:
            price = round(self.product.price * default_currency.coefficient, 2)
        return default_currency.symbol + ' ' + str(price)

# Default data
# try except for initial project
try:
    DEFAULT_CURRENCY = Currency.get_default()
except:
    DEFAULT_CURRENCY = None
